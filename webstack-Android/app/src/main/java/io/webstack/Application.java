package io.webstack;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.view.Window;
import android.view.WindowManager;

public class Application extends AppCompatActivity {

    private Vibrator vibrator;

    public boolean is_dark_mode() {
        int flags = this.getApplicationContext().getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        return flags == Configuration.UI_MODE_NIGHT_YES;
    }

    public void adapt_dark_mode() {
        Window window = this.getWindow();
        if (is_dark_mode()) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.black));
        } else {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            if (is_dark_mode()) {
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            } else {
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (is_dark_mode()) {
            this.getWindow().getContext().setTheme(R.style.Dark);
        }
        super.onCreate(savedInstanceState);
        vibrator = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.application);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        adapt_dark_mode();
        WebView web_view = (WebView) findViewById(R.id.webview);
        web_view.setWebViewClient(new WebViewClient());
        web_view.getSettings().setJavaScriptEnabled(true);
        web_view.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        web_view.getSettings().setSupportMultipleWindows(false);
        web_view.getSettings().setSupportZoom(false);
        web_view.setVisibility(View.GONE);
        Activity activity = this;
        web_view.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        activity);
                if (message.startsWith("set_reference/")) {

                    // No usability in Android.
                    result.cancel();
                    return true;
                } else if (message.startsWith("wake/")) {
                    web_view.setVisibility(View.VISIBLE);
                    web_view.requestFocus(View.FOCUS_DOWN);
                    result.cancel();
                    return true;
                } else if (message.startsWith("sleep/")) {
                    web_view.setVisibility(View.GONE);
                    result.cancel();
                    return true;
                } else if (message.startsWith("url/")) {
                    String website = message.split("url/")[1];
                    Intent browser_intent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
                    startActivity(browser_intent);
                    result.cancel();
                    return true;
                } else if (message.startsWith("haptic/")) {
                    vibrator.vibrate(10);
                    result.cancel();
                    return true;
                } else if (message.startsWith("share/")) {
                    String share_text = message.split("share/")[1];
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, share_text);
                    sendIntent.setType("text/plain");
                    Intent shareIntent = Intent.createChooser(sendIntent, Constants.NAME);
                    startActivity(shareIntent);
                } else if (message.startsWith("sheet/")) {
                    builder.setTitle(message.split("sheet/")[1].split("<param>")[0]);
                    builder.setItems(Utilities.get_parameters(message.split("sheet/")[1].split("<param>")), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            web_view.evaluateJavascript("receive(\"" + message.split("sheet/")[1].split("<param>")[0] + "\", \"" + message.split("sheet/")[1].split("<param>")[which + 1] + "\");", null);
                        }
                    });
                } else if (message.startsWith("alert/")) {
                    builder.setTitle(message.split("alert/")[1].split("<param>")[0]);
                    builder.setMessage(Utilities.get_parameters(message.split("alert/")[1].split("<param>"))[0]);
                } else {
                    builder.setTitle(Constants.NAME).setMessage(message);
                }
                builder.show();
                result.cancel();
                return true;
            }
        });
        web_view.setVerticalScrollBarEnabled(false);
        web_view.setHorizontalScrollBarEnabled(false);
        web_view.loadUrl(Constants.WEBSERVER + "?key=" + Constants.KEY + "&platform=Android" + (is_dark_mode() ? "&darkmode" : ""));
    }

    @Override
    protected void onPostCreate(Bundle saved_instance_state) {
        super.onPostCreate(saved_instance_state);
    }
}