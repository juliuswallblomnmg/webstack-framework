package io.webstack;

public class Utilities {

    public static String[] get_parameters(String[] data) {
        String[] parameters = new String[data.length - 1];
        for (int i = 0; i < data.length - 1; i++) {
            parameters[i] = data[i + 1];
        }
        return parameters;
    }
}
