//
//  ColorCoordination.h
//  Appload
//
//  Created by Julius Wallblom on 2019-09-06.
//  Copyright © 2019 Appload. All rights reserved.
//

#ifndef ColorCoordination_h
#define ColorCoordination_h

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#endif
