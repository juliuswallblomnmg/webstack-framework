#import "ColorCoordination.h"

#ifndef Constants_h
#define Constants_h

static NSString *const url = @"http://192.168.10.188:3111/public/";
static NSString *const key = @"0x0";

#define status_bar_color UIColorFromRGB(0xffffff)
#define status_bar_color_darkmode UIColorFromRGB(0x000000)

#endif
