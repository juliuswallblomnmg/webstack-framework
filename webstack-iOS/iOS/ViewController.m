//
//  ViewController.m
//  Webstack
//
//  Created by Julius Wallblom on 2021-05-11.
//  Copyright © Lesley Cosmetics AB. All rights reserved.
//

#import "ViewController.h"
#import "Constants.h"
#import <WebKit/WebKit.h>
#import <AudioToolbox/AudioServices.h>

@interface ViewController () <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>

@property (nonatomic) WKWebView *web_view;
@property (weak, nonatomic) IBOutlet UIView *reference_point;
@property (nonatomic, strong) UIImpactFeedbackGenerator *impact_feed_generator;

@end

@implementation ViewController

bool dark_mode = false;

- (BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus network_status = [reachability currentReachabilityStatus];
    return network_status != NotReachable;
}

#pragma mark - LifeCycle Methods
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)setup {
    if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark ){
        dark_mode = true;
    } else {
        dark_mode = false;
    }
    self.impact_feed_generator = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleLight];
    [self.impact_feed_generator prepare];
    [self setup_webview];
    [self set_URL: dark_mode ? [NSString stringWithFormat:@"%@%@%@%@%@", url, @"?key=", key, @"&platform=iOS", @"&darkmode"] : [NSString stringWithFormat:@"%@%@%@%@", url, @"?key=", key, @"&platform=iOS"]];
}

- (void)setup_webview {
    self.web_view = [[WKWebView alloc] initWithFrame: [[UIScreen mainScreen] bounds]
                    configuration: [self configure_javascript]];
    self.web_view.UIDelegate = self;
    self.web_view.navigationDelegate = self;
    self.web_view.allowsBackForwardNavigationGestures = YES;
    self.web_view.scrollView.showsHorizontalScrollIndicator = NO;
    self.web_view.scrollView.showsVerticalScrollIndicator = NO;
    self.web_view.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.web_view setHidden:YES];
    [self.view addSubview: self.web_view];
    [self setupWKWebViewConstain: self.web_view];
}

- (void)setupWKWebViewConstain: (WKWebView *)webView {
    webView.translatesAutoresizingMaskIntoConstraints = YES;
    
    NSLayoutConstraint *topConstraint =
    [NSLayoutConstraint constraintWithItem: webView
                                 attribute: NSLayoutAttributeTop
                                 relatedBy: NSLayoutRelationEqual
                                    toItem: self.view
                                 attribute: NSLayoutAttributeTop
                                multiplier: 1.0
                                  constant: 0];
    
    NSLayoutConstraint *bottomConstraint =
    [NSLayoutConstraint constraintWithItem: webView
                                 attribute: NSLayoutAttributeBottom
                                 relatedBy: NSLayoutRelationEqual
                                    toItem: self.view
                                 attribute: NSLayoutAttributeBottom
                                multiplier: 1.0
                                  constant: 0];
    
    NSLayoutConstraint *leftConstraint =
    [NSLayoutConstraint constraintWithItem: webView
                                 attribute: NSLayoutAttributeLeft
                                 relatedBy: NSLayoutRelationEqual
                                    toItem: self.view
                                 attribute: NSLayoutAttributeLeft
                                multiplier: 1.0
                                  constant: 0];
    
    NSLayoutConstraint *rightConstraint =
    [NSLayoutConstraint constraintWithItem: webView
                                 attribute: NSLayoutAttributeRight
                                 relatedBy: NSLayoutRelationEqual
                                    toItem: self.view
                                 attribute: NSLayoutAttributeRight
                                multiplier: 1.0
                                  constant: 0];
    
    NSArray *constraints = @[
                             topConstraint,
                             bottomConstraint,
                             leftConstraint,
                             rightConstraint
                             ];
    
    [self.view addConstraints:constraints];
}

- (void)set_URL:(NSString *)string_url {
    NSURL *url = [[NSURL alloc] initWithString: string_url];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (![self connected] || !data) {
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL: url
                                                      cachePolicy: NSURLRequestReturnCacheDataElseLoad
                                                  timeoutInterval: 5];
        [self.web_view loadRequest: request];
    } else {
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL: url
                                                      cachePolicy: NSURLRequestReloadIgnoringLocalCacheData
                                                  timeoutInterval: 5];
        [self.web_view loadRequest: request];
    }
}

- (WKWebViewConfiguration *)configure_javascript {
    NSString *js_string = @"";
    WKUserScript *user_script = [[WKUserScript alloc] initWithSource: js_string
                                                      injectionTime: WKUserScriptInjectionTimeAtDocumentEnd
                                                   forMainFrameOnly:YES];
    WKUserContentController *wku_content_controller = [WKUserContentController new];
    [wku_content_controller addUserScript: user_script];
    [wku_content_controller addScriptMessageHandler:self name:@"callbackHandler"];
    WKWebViewConfiguration *wk_config = [WKWebViewConfiguration new];
    wk_config.userContentController = wku_content_controller;
    wk_config.limitsNavigationsToAppBoundDomains = YES;
    return wk_config;
}

- (WKWebView *)webView:(WKWebView *)web_view createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigation_action windowFeatures: (WKWindowFeatures *)window_features {
    if (navigation_action.targetFrame != nil &&
        !navigation_action.targetFrame.mainFrame) {
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL: [[NSURL alloc] initWithString: navigation_action.request.URL.absoluteString]];
        [web_view loadRequest: request];
        return nil;
    }
    return nil;
}

- (void)webView:(WKWebView *)web_view runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    if ([message hasPrefix:@"simulate_position/"]) {
        message = [message componentsSeparatedByString:@"simulate_position/"][1];
        NSArray *points = [message componentsSeparatedByString:@"<param>"];
        self.reference_point.frame = CGRectMake([points[0] doubleValue], [points[1] doubleValue], [points[2] doubleValue], [points[3] doubleValue]);
        completionHandler();
    } else if ([message hasPrefix:@"wake/"]) {
        [self.web_view setHidden:NO];
        completionHandler();
    } else if ([message hasPrefix:@"sleep/"]) {
        [self.web_view setHidden:YES];
        completionHandler();
    } else if ([message hasPrefix:@"url/"]) {
        message = [message componentsSeparatedByString:@"url/"][1];
        NSURL *URL = [NSURL URLWithString:message];
        [[UIApplication sharedApplication] openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (!success) {
                 NSLog(@"Error opening URL.");
            }
            completionHandler();
        }];
    } else if ([message hasPrefix:@"haptic/"]) {
        [self.impact_feed_generator impactOccurred];
        completionHandler();
    } else if ([message hasPrefix:@"share/"]) {
        message = [message componentsSeparatedByString:@"share/"][1];
        UIActivityViewController *share_controller = [[UIActivityViewController alloc] initWithActivityItems:@[message] applicationActivities:nil];
        share_controller.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
            [self presentViewController:share_controller animated:YES completion:nil];
        completionHandler();
    } else if ([message hasPrefix:@"sheet/"]) {
        message = [message componentsSeparatedByString:@"sheet/"][1];
        NSArray *actions = [message componentsSeparatedByString:@"<param>"];
        UIAlertController *action_sheet = [UIAlertController alertControllerWithTitle:actions[0] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [action_sheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }]];
        int action_index = 0;
        for (NSString* action in actions) {
            if (action_index != 0 && action_index < [actions count] - 1) {
                [action_sheet addAction:[UIAlertAction actionWithTitle:action style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        NSString *script_source = [NSString stringWithFormat:@"%@%@%@%@%@", @"receive('", actions[0], @"','", actions[action_index], @"');"];
                        [web_view evaluateJavaScript:script_source completionHandler:^(id Result, NSError * error) {
                            if (error != nil) {
                                NSLog(@"Error -> %@", error);
                            }
                        }];
                    }];
                }]];
            }
            action_index++;
        }
        UIPopoverPresentationController *popPresenter = [action_sheet
                                                      popoverPresentationController];
        popPresenter.sourceView = self.reference_point;
        popPresenter.sourceRect = self.reference_point.bounds;
        [self presentViewController:action_sheet animated:YES completion:nil];
        completionHandler();
    } else if ([message hasPrefix:@"alert/"]) {
        message = [message componentsSeparatedByString:@"alert/"][1];
        NSArray *actions = [message componentsSeparatedByString:@"<param>"];
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:
            NSLocalizedString(actions[0], nil) message: actions[1]
                preferredStyle: UIAlertControllerStyleAlert];
        UIAlertAction* default_action = [UIAlertAction actionWithTitle: @"OK"
            style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
              completionHandler();
        }];
        [alert addAction:default_action];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:
            NSLocalizedString([[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey], nil) message: message
                preferredStyle: UIAlertControllerStyleAlert];
        UIAlertAction* default_action = [UIAlertAction actionWithTitle: @"OK"
            style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
              completionHandler();
        }];
        [alert addAction:default_action];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)webView:(WKWebView *)web_view runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *) frame completionHandler: (void (^)(BOOL)) completionHandler {
}

- (void)webView:(WKWebView *)web_view runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler: (void (^)(NSString * _Nullable)) completionHandler {
}

- (void)webView:(WKWebView *)web_view decidePolicyForNavigationAction:(WKNavigationAction *)navigation_action decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webView:(WKWebView *)web_view didStartProvisionalNavigation:(WKNavigation *)navigation {
}

- (void)webView:(WKWebView *)web_view didFailNavigation:(WKNavigation *)navigation withError:(NSError *) error {
}

- (void)webView:(WKWebView *)web_view didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *) error {
}

- (void)userContentController: (WKUserContentController *)user_content_controller didReceiveScriptMessage:(WKScriptMessage *)message {
}

-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
        [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
        self.web_view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, size.width, size.height);
    }

- (void)traitCollectionDidChange:(UITraitCollection *)previous_trait_collection {
    [super traitCollectionDidChange: previous_trait_collection];
    if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) {
        dark_mode = true;
    }
    if (self.traitCollection.userInterfaceStyle == UIUserInterfaceStyleLight) {
        dark_mode = false;
    }
    [self setNeedsStatusBarAppearanceUpdate];
    NSString *scriptSource = [NSString stringWithFormat:@"%@%@%@", @"receive('dark_mode', '", dark_mode ? @"enable" : @"disable", @"')"];
    [self.web_view evaluateJavaScript:scriptSource completionHandler:^(id Result, NSError * error) {
        NSLog(@"Error -> %@", error);
    }];
}
@end
