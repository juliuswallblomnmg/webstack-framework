/*
* The static Application class.
*
* @description
* The application object allows access to the object-oriented
* storyboard from anywhere in the stand-alone script.
*/
var application

/**
* _splash_will_exit.
*
* @description
* Is called when splash screen is about to exit.
*/
function _splash_will_exit() {}

/*
* _view_will_load
*
* @params[0]
* The element that triggered the creation of the ViewController.
*
* @params[1]
* The ViewController template.
*
* @description
* Is called when a new ViewController is about to be introduced.
*/
function _view_will_load(params) {
    const src = params[0]
    const template = params.length > 1 ? params[1] : src.getAttribute('template')
}

/*
* _view_did_load
*
* @params[0]
* The ViewController template.
*
* @params[1]
* The element that triggered the creation of the ViewController.
*
* @description
* Is called when a new ViewController has been introduced.
*/
function _view_did_load(params) {
    const template = params[0]
    const src = params[1]
}

/*
* _on_receive
*
* @key
* The packet key.
*
* @value
* The packet value.
*
* @description
* Is called when the application has received a packet from a native application-module.
*/
function _on_receive(key, value) {}

/*
*
* _click
*
* @params[0]
* The mouse-down event.
*
* @description
* Is called when an element has been clicked
*/
function _click(params) {
    const event = params[0]
    const target_element = event.target || event.srcElement
    if (!target_element) {
        return
    }
}

/*
* _touch_start
*
* @params[0]
* The touch-start event.
*
* @description
* Is called when one or more fingers has touched the display.
*/
function _touch_start(params) {
    const event = params[0]
    const target_element = event.target || event.srcElement
    if (!target_element) {
        return
    }
}

/*
*
* _touch_end
*
* @params[0]
* The touch-start event.
*
* @params[1]
* The current position on the x-axis.
*
* @params[2]
* The current position on the y-axis.
*
* @description
* Is called when one or more fingers has left the display.
*/
function _touch_end(params) {
    const event = params[0]
    const target_element = event.target || event.srcElement
    if (!target_element) {
        return
    }
    const x = params[1]
    const y = params[2]
}

/*
* _touch_move
*
* @params[0]
* The touch-start event.
*
* @params[1]
* The current position on the x-axis.
*
* @params[2]
* The current position on the y-axis.
*
* @description
* Is called when a finger is being moved on the display.
*/
function _touch_move(params) {
    const event = params[0]
    const target_element = event.target || event.srcElement
    if (!target_element) {
        return
    }
    const x = params[1]
    const y = params[2]
}