export default class InteractionController {

    constructor() {
    	this.initial_position = []
    	this.prevent_clicks = false
        this.prevent_touches = false
        this.current_touches = []
        return this
    }

    ongoing_touch_index_by_ID(key) {
        const instance = this
        for (var index = 0; index < instance.current_touches.length; index++) {
            var touch_identifier = instance.current_touches[index].identifier
            if (touch_identifier == key) {
                return index
            }
        }
        return -1
    }
}