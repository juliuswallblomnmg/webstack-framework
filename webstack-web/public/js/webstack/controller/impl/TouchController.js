import InteractionController from '../InteractionController.js'
import ActionSheetPacket from '../../network/impl/packet/impl/outgoing/ActionSheetPacket.js'
import AlertPacket from '../../network/impl/packet/impl/outgoing/AlertPacket.js'
import HapticPacket from '../../network/impl/packet/impl/outgoing/HapticPacket.js'
import URLPacket from '../../network/impl/packet/impl/outgoing/URLPacket.js'
import SimulatedPositionPacket from '../../network/impl/packet/impl/outgoing/SimulatedPositionPacket.js'

var self

export default class TouchController extends InteractionController {

    constructor() {
        super()
        window.addEventListener('touchstart', this.touch_start, false)
        window.addEventListener('touchend', this.touch_end, false)
        window.addEventListener('touchcancel', this.touch_cancel, false)
        window.addEventListener('touchmove', this.touch_move, false)
        self = this
        return self
    }

    touch_start(event) {
        event.preventDefault()
        const target_element = event.target || event.srcElement
        if (target_element.disabled) {
            return
        }
        var touches = event.changedTouches
        for (var index = 0; index < touches.length; index++) {
            const x = touches[index].pageX
            const y = touches[index].pageY
            self.initial_position = [x, y]
            self.current_touches.push(self.copy_touch(touches[index]))
            const viewcontroller_type = application.storyboard.get_focused_viewcontroller().type
            switch (viewcontroller_type) {

                case 'modal':
                    if (x < 25) {
                        application.storyboard.get_focused_viewcontroller().root_element.setAttribute('movable', 'true')
                        continue
                    }
                break

                case 'popup':
                    if (y < 100) {
                        application.storyboard.get_focused_viewcontroller().root_element.setAttribute('movable', 'true')
                        continue
                    }
                break
            }
            
            if (target_element.tagName == 'BUTTON' || target_element.hasAttribute('href')) {
                target_element.classList.add('blink')
                setTimeout(function() {
                    if (target_element.classList.contains('blink')) {
                        target_element.classList.remove('blink')
                    }
                }, 600)
            }
            _touch_start([event])
        }
    }

    touch_end(event) {
        event.preventDefault()
        const target_element = event.target || event.srcElement
        var touches = event.changedTouches
        for (var loop_index = 0; loop_index < touches.length; loop_index++) {
            var touch_index = self.ongoing_touch_index_by_ID(touches[loop_index].identifier)
            if (touch_index >= 0) {
                const x = touches[loop_index].pageX
                const y = touches[loop_index].pageY
                self.current_touches.splice(touch_index, 1)
                const viewcontroller_type = application.storyboard.get_focused_viewcontroller().type
                if (target_element.classList.contains('blink')) {
                    target_element.classList.remove('blink')
                }
                if (self.prevent_touches) {
                    return
                } else if (!self.prevent_touches) {
                    self.prevent_touches = true
                    setTimeout(function() {
                        self.prevent_touches = false
                    }, 300)
                }
                if (application.storyboard.get_focused_viewcontroller().root_element.getAttribute('moving')) {
                    switch (viewcontroller_type) {

                        case 'modal':
                            if (x > 100) {
                                application.storyboard.get_focused_viewcontroller().root_element.removeAttribute('moving')
                                application.storyboard.get_focused_viewcontroller().root_element.classList.add('dismiss-current-viewcontroller-modally')
                                application.storyboard.get_previous_viewcontroller().root_element.classList.add('return-previous-viewcontroller-modally')
                                application.storyboard.get_previous_viewcontroller().overlay.classList.add('fade-out-overlay')

                                setTimeout(function() {
                                    application.storyboard.get_focused_viewcontroller().root_element.className = 'viewcontroller'
                                    application.storyboard.get_focused_viewcontroller().root_element.style.left = '100vw'
                                    application.storyboard.get_previous_viewcontroller().root_element.className = 'viewcontroller'
                                    application.storyboard.get_previous_viewcontroller().root_element.style.left = ''
                                    application.storyboard.get_previous_viewcontroller().overlay.classList.remove('fade-out-overlay')
                                    application.storyboard.get_previous_viewcontroller().overlay.style.opacity = ''
                                    application.storyboard.get_focused_viewcontroller().root_element.outerHTML = ''
                                    application.storyboard.viewcontroller_hierarchy.pop()
                                }, 300)
                            } else {
                                application.storyboard.get_focused_viewcontroller().root_element.removeAttribute('moving')
                                application.storyboard.get_focused_viewcontroller().root_element.classList.add('return-current-viewcontroller-modally')
                                application.storyboard.get_previous_viewcontroller().root_element.classList.add('dismiss-previous-viewcontroller-modally')
                                application.storyboard.get_previous_viewcontroller().overlay.classList.add('fade-in-overlay')

                                setTimeout(function() {
                                    application.storyboard.get_focused_viewcontroller().root_element.className = 'viewcontroller'
                                    application.storyboard.get_focused_viewcontroller().root_element.style.left = ''
                                    application.storyboard.get_previous_viewcontroller().root_element.className = 'viewcontroller'
                                    application.storyboard.get_previous_viewcontroller().root_element.style.left = ''
                                    application.storyboard.get_previous_viewcontroller().overlay.classList.remove('fade-in-overlay')
                                    application.storyboard.get_previous_viewcontroller().overlay.style.opacity = ''
                                }, 300)
                            }
                        break

                        case 'popup':
                            if (y > 200) {
                                application.storyboard.get_focused_viewcontroller().root_element.removeAttribute('moving')
                                application.storyboard.get_focused_viewcontroller().root_element.classList.add('dismiss-current-viewcontroller-popup')
                                application.storyboard.get_previous_viewcontroller().overlay.classList.add('fade-out-overlay')

                                setTimeout(function() {
                                    application.storyboard.get_focused_viewcontroller().root_element.className = 'viewcontroller'
                                    application.storyboard.get_focused_viewcontroller().root_element.style.top = '100vh'
                                    application.storyboard.get_previous_viewcontroller().overlay.classList.remove('fade-out-overlay')
                                    application.storyboard.get_previous_viewcontroller().overlay.style.opacity = ''
                                    application.storyboard.get_focused_viewcontroller().root_element.outerHTML = ''
                                    application.storyboard.viewcontroller_hierarchy.pop()
                                }, 300)
                            } else {
                                application.storyboard.get_focused_viewcontroller().root_element.removeAttribute('moving')
                                application.storyboard.get_focused_viewcontroller().root_element.classList.add('return-current-viewcontroller-popup')
                                application.storyboard.get_previous_viewcontroller().overlay.classList.add('fade-in-overlay')

                                setTimeout(function() {
                                    application.storyboard.get_focused_viewcontroller().root_element.className = 'viewcontroller'
                                    application.storyboard.get_focused_viewcontroller().root_element.style.top = ''
                                    application.storyboard.get_previous_viewcontroller().overlay.classList.remove('fade-in-overlay')
                                    application.storyboard.get_previous_viewcontroller().overlay.style.opacity = ''
                                }, 300)
                            }
                        break
                    }
                }
                if (target_element.tagName == 'INPUT') {
                    target_element.focus()
                }
                if (target_element.tagName != 'INPUT' && (x > self.initial_position[0] + 10 || x < self.initial_position[0] - 10 || y > self.initial_position[1] + 10 || y < self.initial_position[1] - 10)) {
                    return
                }
                if (target_element.tagName == 'BUTTON' || target_element.hasAttribute('href')) {
                    new HapticPacket().send()
                }
                if (target_element.id == 'back-button') {
                    if (application.storyboard.viewcontroller_hierarchy.length > 2) {
                        application.storyboard.get_focused_viewcontroller().dismiss()
                        return
                    }
                }
                if (application.storyboard.get_focused_viewcontroller().type == 'floating') {
                     if (!application.storyboard.get_focused_viewcontroller().root_element.contains(target_element) && target_element.id != 'current-view') {
                        application.storyboard.get_focused_viewcontroller().dismiss()
                        return
                     }
                }
                switch (target_element.getAttribute('type')) {

                    case 'haptic':
                        new HapticPacket().send()
                    return

                    case 'alert':
                        new AlertPacket(target_element.getAttribute('params').split('<param>')).send()
                    return

                    case 'actionsheet':
                        new SimulatedPositionPacket([target_element.getBoundingClientRect().left, target_element.getBoundingClientRect().top, target_element.offsetWidth, target_element.offsetHeight]).send()
                        new ActionSheetPacket(target_element.getAttribute('params').split('<param>')).send()
                    return
                }
                if (target_element.hasAttribute('app-download')) {
                    if (application.request.get('platform') == 'iOS') {
                        new URLPacket(application.config.data['app_store_url']).send()
                    } else {
                        new URLPacket(application.config.data['play_store_url']).send()
                    }
                }
                if (target_element.hasAttribute('href')) {
                    var url_data = target_element.getAttribute('href')
                    if (target_element.getAttribute('href') != 'javascript:void(0)') {
                        target_element.setAttribute('href', 'javascript:void(0)')
                    }
                    new URLPacket(url_data).send()
                }
                if (target_element.getAttribute('template')) {
                    const type = target_element.getAttribute('type') || 'modal'
                    const nav_style = target_element.getAttribute('nav-style')
                    const nav_title = target_element.getAttribute('nav-title')
                    const left_nav_fragment = target_element.getAttribute('left-nav-fragment')
                    const center_nav_fragment = target_element.getAttribute('center-nav-fragment')
                    const right_nav_fragment = target_element.getAttribute('right-nav-fragment')
                    application.storyboard.create_viewcontroller(type, target_element, nav_style, nav_title, [left_nav_fragment, center_nav_fragment, right_nav_fragment])
                }
                _touch_end([event, x, y])
            }
        }
    }

    touch_move(event) {
        event.preventDefault()
        const target_element = event.target || event.srcElement
        var touches = event.changedTouches
        for (var loop_index = 0; loop_index < touches.length; loop_index++) {
            var touch_index = self.ongoing_touch_index_by_ID(touches[loop_index].identifier)
            if (touch_index >= 0) {
                const x = touches[loop_index].pageX
                const y = touches[loop_index].pageY
                self.current_touches.splice(touch_index, 1, self.copy_touch(touches[loop_index]))
                const viewcontroller_type = application.storyboard.get_focused_viewcontroller().type
                if (application.storyboard.get_focused_viewcontroller().root_element.getAttribute('movable')) {
                    switch (viewcontroller_type) {

                        case 'modal':
                            if (x > 25) {
                                application.storyboard.get_focused_viewcontroller().root_element.removeAttribute('movable')
                                application.storyboard.get_focused_viewcontroller().root_element.setAttribute('moving', 'true')
                            }
                        break

                        case 'popup':
                            if (y > 50) {
                                application.storyboard.get_focused_viewcontroller().root_element.removeAttribute('movable')
                                application.storyboard.get_focused_viewcontroller().root_element.setAttribute('moving', 'true')
                            }
                        break
                    }
                } else if (application.storyboard.get_focused_viewcontroller().root_element.getAttribute('moving')) {
                    switch (viewcontroller_type) {

                        case 'modal':
                            const base_offset = screen.width / 4
                            const x_with_offset = x - self.initial_position[0]
                            application.storyboard.get_previous_viewcontroller().root_element.style.left = -base_offset + (x_with_offset / screen.width * base_offset) + 'px'
                            application.storyboard.get_previous_viewcontroller().overlay.style.opacity = 0.25 - (x_with_offset / screen.width)
                            application.storyboard.get_focused_viewcontroller().root_element.style.left = x_with_offset + 'px'
                        break

                        case 'popup':
                            const y_with_offset = y - self.initial_position[1]
                            application.storyboard.get_previous_viewcontroller().overlay.style.opacity = 0.25 - (y_with_offset / screen.height)
                            application.storyboard.get_focused_viewcontroller().root_element.style.top = y_with_offset + 'px'
                        break
                    }
                }
                _touch_move([event, x, y])
            }
        }
    }

    touch_cancel(event) {
        event.preventDefault();
        var touches = event.changedTouches;
        for (var index = 0; index < touches.length; index++) {
            var idx = self.ongoing_touch_index_by_ID(touches[index].identifier)
            self.current_touches.splice(idx, 1)
        }
    }

    copy_touch({ identifier, page_x, page_y }) {
        return { identifier, page_x, page_y }
    }
}