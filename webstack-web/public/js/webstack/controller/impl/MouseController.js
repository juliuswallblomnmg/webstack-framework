import InteractionController from '../InteractionController.js'
import AlertPacket from '../../network/impl/packet/impl/outgoing/AlertPacket.js'
import URLPacket from '../../network/impl/packet/impl/outgoing/URLPacket.js'

export default class TouchController extends InteractionController {

    constructor() {
        super()
        window.addEventListener('click', this.click, false)
        self = this
        return self
    }

    click(event) {
        event.preventDefault()
        const target_element = event.target || event.srcElement
        const viewcontroller_type = application.storyboard.get_focused_viewcontroller().type
        if (target_element.classList.contains('blink')) {
            target_element.classList.remove('blink')
        }
        if (target_element.disabled) {
            return
        }
        if (self.prevent_clicks) {
            return
        } else {
            self.prevent_clicks = true
            setTimeout(function() {
                self.prevent_clicks = false
            }, 300)
        }
        if (application.storyboard.get_focused_viewcontroller().root_element.getAttribute('moving')) {
            switch (viewcontroller_type) {

                case 'modal':
                    if (x > 100) {
                        application.storyboard.get_focused_viewcontroller().root_element.removeAttribute('moving')
                        application.storyboard.get_focused_viewcontroller().root_element.classList.add('dismiss-current-viewcontroller-modally')
                        application.storyboard.get_previous_viewcontroller().root_element.classList.add('return-previous-viewcontroller-modally')
                        application.storyboard.get_previous_viewcontroller().overlay.classList.add('fade-out-overlay')

                        setTimeout(function() {
                            application.storyboard.get_focused_viewcontroller().root_element.className = 'viewcontroller'
                            application.storyboard.get_focused_viewcontroller().root_element.style.left = '100vw'
                            application.storyboard.get_previous_viewcontroller().root_element.className = 'viewcontroller'
                            application.storyboard.get_previous_viewcontroller().root_element.style.left = ''
                            application.storyboard.get_previous_viewcontroller().overlay.classList.remove('fade-out-overlay')
                            application.storyboard.get_previous_viewcontroller().overlay.style.opacity = ''
                            application.storyboard.get_focused_viewcontroller().root_element.outerHTML = ''
                            application.storyboard.viewcontroller_hierarchy.pop()
                        }, 300)
                    } else {
                        application.storyboard.get_focused_viewcontroller().root_element.removeAttribute('moving')
                        application.storyboard.get_focused_viewcontroller().root_element.classList.add('return-current-viewcontroller-modally')
                        application.storyboard.get_previous_viewcontroller().root_element.classList.add('dismiss-previous-viewcontroller-modally')
                        application.storyboard.get_previous_viewcontroller().overlay.classList.add('fade-in-overlay')

                        setTimeout(function() {
                            application.storyboard.get_focused_viewcontroller().root_element.className = 'viewcontroller'
                            application.storyboard.get_focused_viewcontroller().root_element.style.left = ''
                            application.storyboard.get_previous_viewcontroller().root_element.className = 'viewcontroller'
                            application.storyboard.get_previous_viewcontroller().root_element.style.left = ''
                            application.storyboard.get_previous_viewcontroller().overlay.classList.remove('fade-in-overlay')
                            application.storyboard.get_previous_viewcontroller().overlay.style.opacity = ''
                        }, 300)
                    }
                break

                case 'popup':
                    if (y > 200) {
                        application.storyboard.get_focused_viewcontroller().root_element.removeAttribute('moving')
                        application.storyboard.get_focused_viewcontroller().root_element.classList.add('dismiss-current-viewcontroller-popup')
                        application.storyboard.get_previous_viewcontroller().overlay.classList.add('fade-out-overlay')

                        setTimeout(function() {
                            application.storyboard.get_focused_viewcontroller().root_element.className = 'viewcontroller'
                            application.storyboard.get_focused_viewcontroller().root_element.style.top = '100vh'
                            application.storyboard.get_previous_viewcontroller().overlay.classList.remove('fade-out-overlay')
                            application.storyboard.get_previous_viewcontroller().overlay.style.opacity = ''
                            application.storyboard.get_focused_viewcontroller().root_element.outerHTML = ''
                            application.storyboard.viewcontroller_hierarchy.pop()
                        }, 300)
                    } else {
                        application.storyboard.get_focused_viewcontroller().root_element.removeAttribute('moving')
                        application.storyboard.get_focused_viewcontroller().root_element.classList.add('return-current-viewcontroller-popup')
                        application.storyboard.get_previous_viewcontroller().overlay.classList.add('fade-in-overlay')

                        setTimeout(function() {
                            application.storyboard.get_focused_viewcontroller().root_element.className = 'viewcontroller'
                            application.storyboard.get_focused_viewcontroller().root_element.style.top = ''
                            application.storyboard.get_previous_viewcontroller().overlay.classList.remove('fade-in-overlay')
                            application.storyboard.get_previous_viewcontroller().overlay.style.opacity = ''
                        }, 300)
                    }
                break
            }
        }
        if (target_element.tagName == 'INPUT') {
            target_element.focus()
        }
        if (target_element.id == 'back-button') {
            if (application.storyboard.viewcontroller_hierarchy.length > 2) {
                application.storyboard.get_focused_viewcontroller().dismiss()
                return
            }
        }
        if (application.storyboard.get_focused_viewcontroller().type == 'floating') {
             if (!application.storyboard.get_focused_viewcontroller().root_element.contains(target_element) && target_element.id != 'current-view') {
                application.storyboard.get_focused_viewcontroller().dismiss()
                return
             }
        }
        if (target_element.hasAttribute('app-download')) {
            if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
                window.open(application.config.data['app_store_url'], '_blank');
            } else if (/android/i.test(navigator.userAgent)) {
                window.open(application.config.data['play_store_url'], '_blank');
            } else {
                alert('open selection here')
            }
        }
        if (target_element.hasAttribute('href')) {
            var url_data = target_element.getAttribute('href')
            window.open(url_data, '_blank');
        }
        if (target_element.getAttribute('template')) {
            const type = target_element.getAttribute('type') || 'modal'
            const nav_style = target_element.getAttribute('nav-style')
            const nav_title = target_element.getAttribute('nav-title')
            const left_nav_fragment = target_element.getAttribute('left-nav-fragment')
            const center_nav_fragment = target_element.getAttribute('center-nav-fragment')
            const right_nav_fragment = target_element.getAttribute('right-nav-fragment')
            application.storyboard.create_viewcontroller(type, target_element, nav_style, nav_title, [left_nav_fragment, center_nav_fragment, right_nav_fragment])
        }
        _click([event])
    }
}