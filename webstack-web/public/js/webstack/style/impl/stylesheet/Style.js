export default class Style {

    constructor(src) {
        if (src) {
            this.link = document.createElement('link')
            this.link.href = './css/themes/' + src + '.css'
            this.link.rel = 'stylesheet'
        } else {
            this.link = document.createElement('style')
        }
    }

    apply() {
        document.head.appendChild(this.link)
    }

    remove() {
        if (this.link) {
            document.head.removeChild(this.link)
        }
    }
}