import Style from '../Style.js'

export default class DarkMode extends Style {

    constructor() {
        super('./css/styles.darkmode.css')
    }

    should_apply() {
        return application.request.get('darkmode')
    }
}