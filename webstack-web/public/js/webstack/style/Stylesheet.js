import Style from './impl/stylesheet/Style.js'
import DarkMode from './impl/stylesheet/impl/DarkMode.js'

export default class Stylesheet {

    constructor() {
    	var form_data = new FormData()
      	form_data.append('platform', application.request.get('platform'))
      	const data = { username: 'example' };
        fetch('./scripts/compress-css.php', {
		  method: 'POST',
		  body: form_data
		})
		.then(response => response.text())
		.then(data => {
		  	this.style = new Style()
		  	this.style.link.innerHTML = data
		  	this.style.apply()
		  	this.dark_mode = new DarkMode()
	        if (this.dark_mode.should_apply()) {
	            this.dark_mode.apply()
	        }
	        if (application.config.data['theme'].length > 0) {
		        this.theme = new Style(application.config.data['theme'])
		        this.theme.apply()
		    }
		})
    }
}