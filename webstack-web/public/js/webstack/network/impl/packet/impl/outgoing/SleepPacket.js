import OutgoingPacket from '../../OutgoingPacket.js'

export default class SleepPacket extends OutgoingPacket {

    constructor() {
    	super('sleep')
    }
}