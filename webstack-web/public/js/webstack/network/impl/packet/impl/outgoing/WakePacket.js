import OutgoingPacket from '../../OutgoingPacket.js'

export default class WakePacket extends OutgoingPacket {

    constructor() {
    	super('wake')
    }
}