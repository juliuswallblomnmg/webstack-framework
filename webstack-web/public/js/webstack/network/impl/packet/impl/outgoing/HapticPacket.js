import OutgoingPacket from '../../OutgoingPacket.js'

export default class HapticPacket extends OutgoingPacket {

    constructor() {
    	super('haptic')
    }
}