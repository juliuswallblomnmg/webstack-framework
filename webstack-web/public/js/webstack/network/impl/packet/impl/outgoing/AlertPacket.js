import OutgoingPacket from '../../OutgoingPacket.js'

export default class AlertPacket extends OutgoingPacket {

    constructor(params) {
        var value = ''
        if (Array.isArray(params)) {
            for (const param in params) {
                value += (params[param] + '<param>')
            }
        } else {
            value = params
        }
    	super('alert', value)
    }
}