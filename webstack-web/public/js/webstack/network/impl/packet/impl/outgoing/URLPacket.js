import OutgoingPacket from '../../OutgoingPacket.js'

export default class URLPacket extends OutgoingPacket {

    constructor(href) {
    	super('url', href)
    }
}