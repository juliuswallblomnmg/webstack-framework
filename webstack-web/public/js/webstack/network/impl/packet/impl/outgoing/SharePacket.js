import OutgoingPacket from '../../OutgoingPacket.js'

export default class SharePacket extends OutgoingPacket {

    constructor(content) {
    	super('share', content)
    }
}