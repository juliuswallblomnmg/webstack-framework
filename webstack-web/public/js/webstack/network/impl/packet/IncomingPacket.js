function receive(key, value) {
    if (key == 'dark_mode') {
        if (value == 'enable') {
            application.display.style.dark_mode.apply()
        } else if (value == 'disable') {
            application.display.style.dark_mode.remove()
        }
    }
    _on_receive(key, value)
}