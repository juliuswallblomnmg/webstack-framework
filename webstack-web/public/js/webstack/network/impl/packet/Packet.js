export default class Packet {

    constructor(key, value) {
    	this.key = key
        this.value = value || ''
    }
}