import Packet from './Packet.js'

export default class OutgoingPacket extends Packet {

    constructor(key, value) {
    	super(key, value)
    }

    send() {
        alert(this.key + '/' + this.value)
    }
}