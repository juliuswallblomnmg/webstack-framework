export default class Request {

	get(key) {
	    var value = null, tmp = []
	    location.search
        .substr(1)
        .split("&")
        .forEach(function(item) {
            tmp = item.split("=")
            if (tmp[0] === key) {
                value = decodeURIComponent(tmp[1])
            }
        })
        if (key == 'platform' && !value) {
        	value = 'Browser'
        }
	    return value
	}
}