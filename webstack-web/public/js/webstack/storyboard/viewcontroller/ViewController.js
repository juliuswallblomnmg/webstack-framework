import Component from '../components/Component.js'
import MobileNav from './impl/nav/impl/MobileNav.js'
import BrowserReturn from '../components/impl/BrowserReturn.js'

export default class ViewController {

    constructor(params) {
        this.type = params[0]
        this.title = params[2]
        this.root_element = document.createElement('div')
        if (application.request.get('platform') != 'Browser') {
            this.nav = new MobileNav(this, params[1])
            this.root_element.appendChild(this.nav.root_element)
        }
        this.overlay = document.createElement('div')
        this.view = document.createElement('div')
        this.root_element.classList.add('viewcontroller')
        this.overlay.classList.add('overlay')
        this.view.classList.add('view')
        this.view.id = 'current-view'
        this.view.classList.add('view')
        this.trigger = params[3]
        this.root_element.appendChild(this.overlay)
        this.root_element.appendChild(this.view)
    }

    async populate(template) {
        const instance = this
        var response = await fetch('./templates/viewcontrollers/' + template + '.php')
        if (!response.ok) {
            throw new Error('Error populating ViewController: ' + response.status)
        }
        const tmp_HTML = document.createElement('div')
        tmp_HTML.innerHTML = await response.text()
        if (application.request.get('platform') == 'Browser' && this.type != 'splash' && this.type != 'root' && this.type != 'block') {
           var browser_return = new BrowserReturn()
           browser_return.populate()
           instance.view.appendChild(browser_return.element)
           instance.view.appendChild(tmp_HTML)
        } else {
            this.view.appendChild(tmp_HTML)
        }
    }

    component(template, parent) {
        var component = new Component(template)
        var overlay = document.createElement('div')
        overlay.classList.add('dynamic-overlay')
        var loading_spinner_container = document.createElement('div')
        loading_spinner_container.classList.add('loading-spinner-container')
        var loading_spinner = document.createElement('div')
        loading_spinner.classList.add('loading-spinner')
        loading_spinner_container.appendChild(loading_spinner)
        overlay.appendChild(loading_spinner_container)
        parent.innerHTML = overlay.innerHTML
        component.populate().then(() => {
            setTimeout(function() {
                parent.innerHTML = component.element.innerHTML
            }, 300)
        })
    }

    introduce() {
        const instance = this
        application.storyboard.get_previous_viewcontroller().root_element.children[1].classList.add('fade-in-overlay')
        this.root_element.classList.add('introduce-' + (application.request.get('platform') == 'Browser' ? 'browser' : this.type))
        setTimeout(function() {
            instance.root_element.classList.remove('introduce-' + (application.request.get('platform') == 'Browser' ? 'browser' : this.type))
            application.storyboard.get_previous_viewcontroller().root_element.children[1].classList.remove('fade-in-overlay')
            _view_did_load([instance.trigger.getAttribute('template'), instance.trigger])
        }, 300)
    }

    dismiss() {
        const instance = this
        application.storyboard.get_previous_viewcontroller().overlay.classList.add('fade-out-overlay')
        this.root_element.classList.add('dismiss-' + (application.request.get('platform') == 'Browser' ? 'browser' : this.type))
        setTimeout(function() {
            application.storyboard.get_previous_viewcontroller().overlay.style.opacity = ''
            application.storyboard.get_previous_viewcontroller().overlay.classList.remove('fade-out-overlay')
            instance.root_element.classList.remove('dismiss-' + (application.request.get('platform') == 'Browser' ? 'browser' : instance.type))
            instance.root_element.outerHTML = ''
            application.storyboard.viewcontroller_hierarchy.pop()
        }, 300)
    }
}