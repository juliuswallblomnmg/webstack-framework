import ViewController from '../ViewController.js';

export default class FloatingViewController extends ViewController {

    constructor(params) {
        super(params)
        this.view.parentNode.classList.add('floating')
        this.view.parentNode.style.height = this.trigger.getAttribute('height')
        this.view.parentNode.style.minHeight = this.trigger.getAttribute('height')
        this.view.parentNode.style.maxHeight = this.trigger.getAttribute('height')
        this.nav.root_element.style.paddingTop = '0'
        return this
    }

    introduce() {
        const instance = this
        application.storyboard.get_previous_viewcontroller().root_element.children[1].classList.add('fade-in-overlay')
        this.root_element.classList.add('introduce-' + this.type)
        setTimeout(function() {
            instance.root_element.classList.remove('introduce-' + instance.type)
            application.storyboard.get_previous_viewcontroller().root_element.children[1].classList.remove('fade-in-overlay')
            application.storyboard.get_previous_viewcontroller().overlay.style.opacity = '0.25'
            _view_did_load([instance.trigger.getAttribute('template'), instance.trigger])
        }, 300)
    }
}