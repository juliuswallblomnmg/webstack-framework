import ViewController from '../ViewController.js';

export default class RootViewController extends ViewController {

    constructor(params) {
        super(params)
        this.view.id = 'root-view'
        return this
    }

    introduce() {
        const instance = this
        this.root_element.classList.add('introduce-' + (application.request.get('platform') == 'Browser' ? 'browser' : 'popup'))
        setTimeout(function() {
            instance.root_element.classList.remove('introduce-' + (application.request.get('platform') == 'Browser' ? 'browser' : 'popup'))
            application.storyboard.get_previous_viewcontroller().root_element.remove()
            _view_did_load([instance.type, instance.trigger])
        }, 300)
    }
}