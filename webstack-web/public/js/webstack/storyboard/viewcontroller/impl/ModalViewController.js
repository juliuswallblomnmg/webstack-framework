import ViewController from '../ViewController.js';

export default class ModalViewController extends ViewController {

    constructor(params) {
        super(params)
        return this
    }

    introduce() {
        const instance = this
        application.storyboard.get_previous_viewcontroller().root_element.children[1].classList.add('fade-in-overlay')
        application.storyboard.get_previous_viewcontroller().root_element.classList.add('exit-' + this.type)
        this.root_element.classList.add('introduce-' + (application.request.get('platform') == 'Browser' ? 'browser' : this.type))
        setTimeout(function() {
            instance.root_element.classList.remove('introduce-' + (application.request.get('platform') == 'Browser' ? 'browser' : instance.type))
            application.storyboard.get_previous_viewcontroller().root_element.children[1].classList.remove('fade-in-overlay')
            application.storyboard.get_previous_viewcontroller().root_element.classList.remove('exit-' + instance.type)
            _view_did_load([instance.trigger.getAttribute('template'), instance.trigger])
        }, 300)
    }
}