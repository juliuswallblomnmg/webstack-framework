import Component from '../../components/Component.js'
import ViewController from '../ViewController.js';
import BrowserReturn from '../../components/impl/BrowserReturn.js'

export default class BrowserViewController extends ViewController {

    constructor(params) {
        super(params)
        return this
    }
    
    introduce() {
        _view_did_load([this.trigger.getAttribute('template'), this.trigger])
    }

    async populate(template) {
        const instance = this
        var overlay = document.createElement('div')
        overlay.classList.add('dynamic-overlay')
        var loading_spinner = document.createElement('div')
        loading_spinner.classList.add('loading-spinner')
        overlay.appendChild(loading_spinner)
        instance.view.innerHTML = ''
        instance.view.appendChild(overlay)
        var response = await fetch('./templates/viewcontrollers/' + template + '.php')
        if (!response.ok) {
            throw new Error('Error populating ViewController: ' + response.status)
        }
        const tmp_HTML = document.createElement('div')
        tmp_HTML.innerHTML = await response.text()
        var browser_return = new BrowserReturn()
        browser_return.populate().then(() => {
            setTimeout(function() {
                var tmp_element = document.createElement('div')
                tmp_element.appendChild(browser_return.element)
                tmp_element.appendChild(tmp_HTML)
                instance.view.innerHTML = tmp_element.innerHTML
            }, 300)
        })
    }

    dismiss() {
        const instance = this
        var overlay = document.createElement('div')
        overlay.classList.add('dynamic-overlay')
        var loading_spinner = document.createElement('div')
        loading_spinner.classList.add('loading-spinner')
        overlay.appendChild(loading_spinner)
        instance.view.innerHTML = ''
        instance.view.appendChild(overlay)
        setTimeout(function() {
            instance.root_element.outerHTML = ''
            application.storyboard.viewcontroller_hierarchy.pop()
        }, 300)
    }
}