import Fragment from '../Fragment.js';

export default class LeftFragment extends Fragment {

    constructor(parent_viewcontroller, src) {
        super(parent_viewcontroller, src)
        this.root_element.classList.add('nav-fragment')
        return this
    }

    async append() {
        super.append()
        if (!this.src) {
            var close_viewcontroller_element = document.createElement('button')
            close_viewcontroller_element.id = 'back-button'
            close_viewcontroller_element.classList.add('nav-button')
            if (this.parent_viewcontroller.type == 'popover' || this.parent_viewcontroller.type == 'floating') {
                close_viewcontroller_element.innerHTML = '􀆄'
            } else if (this.parent_viewcontroller.type == 'popup') {
                close_viewcontroller_element.innerHTML = '􀆈'
            } else if (this.parent_viewcontroller.type == 'modal') {
                close_viewcontroller_element.innerHTML = '􀆉'
            } else {
                close_viewcontroller_element.disabled = true
            }
            this.root_element.appendChild(close_viewcontroller_element)
        }
    }
}