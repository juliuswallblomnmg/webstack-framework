import Fragment from '../Fragment.js';

export default class RightFragment extends Fragment {

    constructor(parent_viewcontroller, src) {
        super(parent_viewcontroller, src)
        this.root_element.classList.add('nav-fragment')
        this.root_element.classList.add('right')
        return this
    }
}