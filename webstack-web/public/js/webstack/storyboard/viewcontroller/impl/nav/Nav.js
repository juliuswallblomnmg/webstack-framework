import Fragment from './impl/fragment/Fragment.js';
import LeftFragment from './impl/fragment/impl/LeftFragment.js';
import CenterFragment from './impl/fragment/impl/CenterFragment.js';
import RightFragment from './impl/fragment/impl/RightFragment.js';

export default class Nav {

    constructor(parent, style) {
        if (parent) {
            this.title = parent.title
            this.type = parent.type
        }
        this.root_element = document.createElement('div')
        this.root_element.id = 'nav-bar'
        this.root_element.classList.add('nav-bar')
        if (style) {
            this.style = style
            this.root_element.classList.add(this.style)
        }
        return this
    }

    async populate(fragments) {
        if (fragments) {
            this.fragments = [new LeftFragment(this, fragments[0]), new CenterFragment(this, fragments[1]), new RightFragment(this, fragments[2])]
            for (var index in fragments) {
                this.root_element.appendChild(this.fragments[index].root_element)
                await this.fragments[index].append()
            }
        }
    }
}