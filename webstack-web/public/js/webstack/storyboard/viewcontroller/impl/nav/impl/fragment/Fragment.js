export default class Fragment {

    constructor(parent_viewcontroller, src) {
        this.parent_viewcontroller = parent_viewcontroller
        this.src = src
        this.root_element = document.createElement('div')
    }

    async populate() {
        if (this.src) {
            var response = await fetch('./templates/viewcontrollers/nav/' + this.src + '.php')
            if (!response.ok) {
                throw new Error('Error populating Fragment: ' + response.status)
            }
            var data = await response.text()
            this.root_element.innerHTML = data
        }
    }

    async append() {
        await this.populate()
    }
}