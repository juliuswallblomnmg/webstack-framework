import Fragment from '../Fragment.js';

export default class CenterFragment extends Fragment {

    constructor(parent_viewcontroller, src) {
        super(parent_viewcontroller, src)
        this.root_element.classList.add('nav-fragment')
        this.root_element.classList.add('center')
        return this
    }

    async append() {
        super.append()
        if (!this.src) {
            var viewcontroller_title_element = document.createElement('p')
            viewcontroller_title_element.id = 'viewcontroller-title'
            viewcontroller_title_element.innerHTML = this.parent_viewcontroller.title
            viewcontroller_title_element.classList.add('viewcontroller-title')
            this.root_element.appendChild(viewcontroller_title_element)
        }
    }
}