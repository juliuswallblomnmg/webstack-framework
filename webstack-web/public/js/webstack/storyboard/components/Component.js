export default class Component {

    constructor(template) {
        this.template = template
        this.element = document.createElement('div')
        return this
    }

    async populate() {
        let response = await fetch('./templates/components/' + this.template + '.php')
        if (!response.ok) {
            throw new Error('Error populating Component: ' + response.status)
        }
        this.element.innerHTML = await response.text()
    }
}