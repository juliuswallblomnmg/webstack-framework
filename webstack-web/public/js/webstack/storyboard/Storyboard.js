import Stylesheet from '../style/Stylesheet.js'
import ViewController from './viewcontroller/ViewController.js'
import SplashViewController from './viewcontroller/impl/SplashViewController.js'
import RootViewController from './viewcontroller/impl/RootViewController.js'
import ActionSheetPacket from '../network/impl/packet/impl/outgoing/ActionSheetPacket.js'

export default class Storyboard {

    constructor() {
        this.stylesheet = new Stylesheet()
        this.viewcontroller_hierarchy = new Array()
        this.root_element = document.getElementById('storyboard')
        this.splash_viewcontroller = new SplashViewController(['splash'])
        this.splash_viewcontroller.populate('splash').then(() => {
            this.root_element.appendChild(this.splash_viewcontroller.root_element)
            _view_did_load(['splash', null])
        })
        this.viewcontroller_hierarchy.push(this.splash_viewcontroller)
    }

    introduce_root() {
        _splash_will_exit()
        this.root_viewcontroller = new RootViewController(['root'])
    }

    push_root() {
        this.viewcontroller_hierarchy.push(this.root_viewcontroller)
        this.root_viewcontroller.introduce()
    }

    get_focused_viewcontroller() {
        return this.viewcontroller_hierarchy[this.viewcontroller_hierarchy.length - 1]
    }

    get_previous_viewcontroller() {
        return this.viewcontroller_hierarchy[this.viewcontroller_hierarchy.length - 2]
    }
}