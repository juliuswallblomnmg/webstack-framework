import Storyboard from '../Storyboard.js'
import RootViewController from '../viewcontroller/impl/RootViewController.js'
import ModalViewController from '../viewcontroller/impl/ModalViewController.js'
import PopupViewController from '../viewcontroller/impl/PopupViewController.js'
import PopoverViewController from '../viewcontroller/impl/PopoverViewController.js'
import FloatingViewController from '../viewcontroller/impl/FloatingViewController.js'
import BlockViewController from '../viewcontroller/impl/BlockViewController.js'
import WakePacket from '../../network/impl/packet/impl/outgoing/WakePacket.js'

export default class MobileStoryboard extends Storyboard {

    constructor() {
        super()
    }

    introduce_root() {
    	super.introduce_root()
        new WakePacket().send()
        this.root_viewcontroller.nav.populate(['root_nav_fragment_left', 'root_nav_fragment_center', 'root_nav_fragment_right']).then(() => {
            this.root_viewcontroller.populate('root').then(() => {
                this.root_element.appendChild(this.root_viewcontroller.root_element)
            })
        })
        this.push_root()
    }

    create_viewcontroller(type, trigger, nav_style, nav_title, fragments) {
        var viewcontroller
        switch (type) {

            case 'modal':
                viewcontroller = new ModalViewController([type, nav_style, nav_title, trigger])
            break

            case 'popup':
                viewcontroller = new PopupViewController([type, nav_style, nav_title, trigger])
            break

            case 'popover':
                viewcontroller = new PopoverViewController([type, nav_style, nav_title, trigger])
            break

            case 'floating':
                viewcontroller = new FloatingViewController([type, nav_style, nav_title, trigger])
            break

            case 'block':
                viewcontroller = new BlockViewController([type, nav_style, nav_title, trigger])
            break
        }
        const instance = this
        this.viewcontroller_hierarchy.push(viewcontroller)
        viewcontroller.nav.populate(fragments).then(() => {
            viewcontroller.populate(trigger.getAttribute('template')).then(() => {
                instance.root_element.appendChild(viewcontroller.root_element)
                _view_will_load([trigger])
                viewcontroller.introduce()
            })
        })
    }
}