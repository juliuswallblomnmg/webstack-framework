import Storyboard from '../Storyboard.js'
import AppDownloadBanner from '../../storyboard/components/impl/AppDownloadBanner.js'
import BrowserNav from '../viewcontroller/impl/nav/impl/BrowserNav.js'
import RootViewController from '../viewcontroller/impl/RootViewController.js'
import BrowserViewController from '../viewcontroller/impl/BrowserViewController.js'

export default class BrowserStoryboard extends Storyboard {

    constructor() {
        super()
        if (application.config.data['app_store_url'].length > 0 && application.config.data['play_store_url'].length > 0 && /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            this.app_download_banner = new AppDownloadBanner()
            this.app_download_banner.populate()
            this.root_element.appendChild(this.app_download_banner.element)
        }
        this.nav = new BrowserNav()
        this.root_element.appendChild(this.nav.root_element)
    }

    introduce_root() {
        super.introduce_root()
        this.nav.populate(['root_nav_fragment_left', 'root_nav_fragment_center', 'root_nav_fragment_right']).then(() => {
            this.root_viewcontroller.populate('root').then(() => {
                this.root_element.appendChild(this.root_viewcontroller.root_element)
            })
        })
        this.push_root()
    }

    create_viewcontroller(type, trigger, nav_style, nav_title, fragments) {
        var viewcontroller = new BrowserViewController([type, nav_style, nav_title, trigger])
        const instance = this
        this.viewcontroller_hierarchy.push(viewcontroller)
        viewcontroller.populate(trigger.getAttribute('template')).then(() => {
            instance.root_element.appendChild(viewcontroller.root_element)
            _view_will_load([trigger])
            viewcontroller.introduce()
        })
    }
}