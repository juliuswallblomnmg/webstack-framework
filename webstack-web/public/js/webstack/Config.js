export default class Config {

    constructor() {
        this.data = Array()
    }

    async load() {
        var response = await fetch('../config.json')
        if (!response.ok) {
            throw new Error('Error fetching Config ' + response.status)
        }
        var json = await response.text()
        this.data = JSON.parse(json)
    }
}