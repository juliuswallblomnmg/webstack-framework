import Config from './Config.js'
import Request from './network/Request.js'
import MouseController from './controller/impl/MouseController.js'
import TouchController from './controller/impl/TouchController.js'
import BrowserStoryboard from './storyboard/impl/BrowserStoryboard.js'
import MobileStoryboard from './storyboard/impl/MobileStoryboard.js'

export default class Application {

    constructor() {
        application = this
        this.config = new Config()
        this.config.load().then(() => {
            this.request = new Request()
            this.interaction_controller = application.request.get('platform') != 'Browser' && /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? new TouchController() : new MouseController()
	        this.storyboard = application.request.get('platform') == 'Browser' ? new BrowserStoryboard() : new MobileStoryboard()
            this.storyboard.introduce_root()
        })
    }
}