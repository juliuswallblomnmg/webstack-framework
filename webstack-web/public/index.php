<?php

    /*
    * Starts the session allowing access to $_SESSION.
    */
    session_start();

    /*
    * Makes the initial database-connection.
    */
    include './sql/SQLConnection.php';
    $sql = new SQLConnection();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>App</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover"> 
  <script src="js/script.js"></script>
  <script src="js/webstack/network/impl/packet/IncomingPacket.js"></script>
  <script src="js/webstack/Module.js" type="module"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic"]  }});</script>
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
</head>
<body class="body">
  <div id="storyboard"></div>
</body>
</html>