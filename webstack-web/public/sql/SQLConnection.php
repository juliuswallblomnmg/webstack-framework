<?php

include 'config.php';

class SQLConnection {

    private $conn;
    private $stmt;

    public function __construct() {
        $this->conn = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_SCHEMA . ';charset=utf8', DB_USER, DB_PASSWORD);
    }

    private function insert($table, $data) {
        $keys = array_keys($data);
        $keys = array_map('escape_mysql_identifier', $keys);
        $fields = implode(',', $keys);
        $table = escape_mysql_identifier($table);
        $placeholders = str_repeat('?,', count($keys) - 1) . '?';
        $stmt = 'INSERT INTO $table ($fields) VALUES ($placeholders)';
        $this->conn->prepare($stmt)->execute(array_values($data));
    }

    private function select($table) {
        $stmt = $this->conn->prepare('SELECT * FROM $table');
        $stmt->execute(); 
        return $stmt->fetchAll();
    }
    
    private function select_by_index($table, $index) {
        $stmt = $this->conn->prepare('SELECT * FROM $table WHERE index=:index');
        $stmt->execute(['index' => $index]); 
        return $stmt->fetch();
    }
}