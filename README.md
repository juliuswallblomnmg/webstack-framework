Copyright © 2023 Julius Wallblom under the [AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html) license.

**Webstack, an extremely lightweight [JavaScript](https://en.wikipedia.org/wiki/JavaScript) mobile & browser application framework**

Webstack is a modular, extremely lightweight **(<300KB)** mobile & browser PWA framework built for web-developers intended to bridge the native development platforms into one built on webstack technology. Webstack uses no external libraries, dependencies or frameworks. It is written entirely in [JavaScript](https://en.wikipedia.org/wiki/JavaScript), [HTML](https://en.wikipedia.org/wiki/HTML), [CSS](https://en.wikipedia.org/wiki/CSS), [Objective-C](https://en.wikipedia.org/wiki/Objective-C) & [Java](https://en.wikipedia.org/wiki/Java).

Setting up a workspace takes **less than one minute**.

---

![Preview](https://nineteenmediagroup.com/webstack/webstack_gif_320.gif)


---

## **About**

## Support (as of v2):
* Storyboard with ViewController hierarchy
* ViewControllers (splash, root, modal, popover, popup & floating)
* Native dark-mode support
* Application themes
* Stylesheet lazyloading
* ServiceWorker offline-support
* Touch-control (start, move, cancel & end)
* Dynamic & animated storyboard-navigation
* Wake/sleep
* Alerts
* ActionSheets with iOS position simulation (for iPads)
* Haptic feedback
* Content sharing across native applications
* Opening external URLs (no modifications needed compared to basic web-development)
* **V2** Browser (desktop) application support!

---

## **1. Getting Started**

To get started with Webstack, clone the repository and then `cd` into **webstack-framework/www** using the terminal.

Execute `docker-compose build` to build the [Dockerfile](https://docker.com/), then start the container by executing `docker-compose up -d`.

The web-server for the mobile application should now be running and available at [http://localhost:3111](http://localhost:3111).

Finally, set the IP-address constant in the respective native mobile application directories to your local static IP-address. Build the application in [Xcode](https://developer.apple.com/xcode/) or [Android Studio](https://developer.android.com/studio/install) and you're ready to develop your mobile PWA using Webstack.
For the iOS workspace, you also have to make sure the WKAppBoundDomains dictionary in info.plist matches your local static IP-address.

---

## **2. Development**

**A)**

To edit the root ViewController for your mobile application, see **webstack-framework/webstack-web/templates/viewcontrollers/root.html**.

To edit the navigation bar of the root ViewController, see **webstack-framework/templates/viewcontrollers/nav/root_nav_fragment_left/right/center.html**.

To create new ViewControllers, add HTML-templates in the **webstack-framework/webstack-web/html/templates/** directory.

To link an element to a ViewController, you add attributes to the element that you want opening the viewcontroller.

After creating a new ViewController (as described above), you can link to it by adding the `template` **(template="your_ViewController_name")** attribute.

To add a title to your ViewController, assign the `title` **(title="your_title")** attribute.

To add a style to your ViewController's navigation bar, assign the `nav-style` **(nav-style="your_css_object")** attribute.

To set the type of the ViewController, assign the `type` **(type="modal/popup/popover/floating")** attribute.

To assign navigation fragments to your viewcontroller, create a HTML-template in **webstack-framework/www/templates/viewcontrollers/nav/** and assign the `alignment-nav-fragment` **(left-nav-fragment="your_fragment")** attribute to the linking element.

To create custom styles for your own HTML elements, see **webstack-framework/webstack-web/css/styles.css/styles.Android.css/styles.iOS.css**.

**B)**

In order to script your application, navigate to **webstack-framework/webstack-web/js/script.js**. This Script is pre-populated with functions that lets you hook into the lifecycle of the application.